angular.module('app', ['ui.router', 'chart.js'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: './app/views/dashboard.html',
                controller: 'OverrideCtrl'
            })
            .state('news', {
                url: '/news',
                templateUrl: './app/views/news.html',
            });
        $urlRouterProvider.otherwise('/dashboard');
    })
    .constant('Config', {
        interval: 6000
    })
    .controller('OverrideCtrl', ['Config', '$scope', 'ActivityService', '$interval', function (Config, $scope, ActivityService, $interval) {
        $scope.data = [];
        $scope.items = [];
        $scope.options = {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        format: "HH:mm",
                        unit: 'minute',
                        unitStepSize: 5,
                        displayFormats: {
                            'minute': 'HH:mm',
                            'hour': 'HH:mm'
                        }
                    }
                }]
            }
        }
        ActivityService.getUsers().then(response => {
            let resp = response;
            $interval(function () {
                let random = resp.map(item => {
                    var randomObject = {
                        zoneId: item.zoneId,
                        data: {
                            count: _.random(1, 10),
                            speed: _.random(30, 100),
                            time: moment().add(5, 'minute').toDate()
                        }
                    };
                    return randomObject;
                })
                $scope.data.push(random);
                let realTime = _.zip.apply(_, $scope.data);

                ////////////
                let barDataSet = [];

                realTime.forEach(item => {
                    let count = 0;
                    item.forEach((subItem) => {
                        count += subItem.data.count;
                    });
                    barDataSet.push(count);
                })
                $scope.barLabels = ['1', '2', '3', '4', '5'];
                $scope.barSeries = ['Count'];

                $scope.barDataSet = barDataSet;
                ////////////

                ////////////
                let speedAverage = [];

                realTime.forEach(item => {
                    let count = 0;
                    item.forEach((subItem) => {
                        count += subItem.data.speed;
                    });
                    speedAverage.push(count / item.length);
                })
                $scope.doughnutLabels = ["1", "2", "3", "4", "5"];
                $scope.doughnutDataSet = speedAverage;

                //////////////
                let intervals = [];
                let speeds = [];
                realTime.forEach(item => {
                    let speed = [];
                    item.forEach((subItem) => {
                        intervals.push(subItem.data.time);
                        speed.push(subItem.data.speed);
                    })
                    speeds.push(speed);
                })
                $scope.lineLabels = intervals;
                $scope.lineData = speeds;
                let lineDataSets = [];
                realTime.forEach((item) => {
                    let datasetOverride = {
                        label: 'Live View',
                        borderWidth: 3,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        type: 'line',
                        fill: false
                    }
                    lineDataSets.push(datasetOverride);
                });
                $scope.lineDataSets = lineDataSets;
            }, Config.interval);
        });
    }])
    .service('ActivityService', function ($http) {
        let service = {
            getUsers: function () {
                return $http.get('./app/data/activity-data.json').then(function (resp) {
                    return resp.data;
                });
            }
        }
        return service;
    })